﻿using System;
using Volo.Abp.DependencyInjection;

namespace Market.Proxy
{
    public class HelloWorldService : ITransientDependency
    {
        public void SayHello()
        {
            Console.WriteLine("Hello World!");
        }
    }
}
