﻿using Volo.Abp.Autofac;
using Volo.Abp.Modularity;

namespace Market.Proxy
{

    [DependsOn(
        typeof(AbpAutofacModule)
    )]
    public class ProxyModule : AbpModule
    {

    }
}
